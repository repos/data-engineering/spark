#!/bin/bash

#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#  .
#  https://www.apache.org/licenses/LICENSE-2.0
#  .
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#  .
#  On Debian systems, the complete text of the Apache version 2.0 license
#  can be found in "/usr/share/common-licenses/Apache-2.0".

# This script creates a number of Debian packages, each containing a version
# of the spark external shuffler service for YARN. The binary jar file that
# provides the yarn shuffler service is extracted from the spark build environment
# container image, which is hosted on https://docker-registry.wikimedia.org

set -e
set -x

SPARK_VERSIONS=(
3.1.2
3.3.2
3.4.1
3.5.3
)

BUILD_VERSION=3

for PATCH_VERSION in ${SPARK_VERSIONS[@]}; do
  # Create a directory for the build and debian files
  MINOR_VERSION=${PATCH_VERSION%\.*}
  mkdir -p ${MINOR_VERSION}/debian
  # Pull the spark-build image corresponding to this version of spark
  docker pull docker-registry.wikimedia.org/spark${MINOR_VERSION}-build:latest
  # Create a named container from the image
  docker create --name spark-build-${MINOR_VERSION} -it docker-registry.wikimedia.org/spark${MINOR_VERSION}-build:latest bash
  # Extract the spark shuffler jar and save it to the build directory
  docker cp spark-build-${MINOR_VERSION}:/usr/src/spark/common/network-yarn/target/scala-2.12/spark-${PATCH_VERSION}-yarn-shuffle.jar ./${MINOR_VERSION}/
  # Clean up the container
  docker rm spark-build-${MINOR_VERSION}

  # Create the required Debian build files from templates, replacing version tokens
  for FILE in dirs install postinst prerm; do
    sed -e "s/%MINOR_VERSION%/${MINOR_VERSION}/g" \
        -e "s/%PATCH_VERSION%/${PATCH_VERSION}/g" \
      debian/spark-MINOR_VERSION-yarn-shuffle.${FILE}.template > ${MINOR_VERSION}/debian/spark-${MINOR_VERSION}-yarn-shuffle.${FILE}
  done

  sed -e "s/%MINOR_VERSION%/${MINOR_VERSION}/g" \
      debian/control.template > $MINOR_VERSION/debian/control

  sed -e "s/%PATCH_VERSION%/${PATCH_VERSION}/g" \
      -e "s/%MINOR_VERSION%/${MINOR_VERSION}/g" \
      -e "s/%BUILD_VERSION%/${BUILD_VERSION}/" \
      debian/changelog.template > $MINOR_VERSION/debian/changelog

  cat debian/rules.template > $MINOR_VERSION/debian/rules
  chmod +x $MINOR_VERSION/debian/rules

  # Enter the build directory for this version
  cd ${MINOR_VERSION}
  
  # Execute the package build
  DIST=bullseye pdebuild

  # Return to the upper-level directory
  cd ../

  # Remove the jar file
  rm ${MINOR_VERSION}/spark-${PATCH_VERSION}-yarn-shuffle.jar
  # Clean up the remaining rendered build files
  rm ${MINOR_VERSION}/debian/{changelog,rules,control}
  rm ${MINOR_VERSION}/debian/spark-${MINOR_VERSION}-yarn-shuffle.{dirs,install,postinst,prerm}
done