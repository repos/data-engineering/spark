# Spark Shuffler on YARN - Debian builds

This script produces a set of Debian packages for installing the Spark shuffler service on Debian.

The spark distributions have already built built and the artifacts are hosted on docker-registry.wikimedia.org.

This script simply carries out the following steps:

- downloads the spark-build image for each version
- extracts the jar file for the spark-shuffler service
- creates a set of Debian control files and scripts
- builds the package for each of the versions of spark listed in the SPARK_VERSIONS variable

The script should be run from a WMF build server, for example:

```
ssh build2001.codfw.wmnet
git clone https://gitlab.wikimedia.org/repos/data-engineering/spark
cd spark
./build_spark_shuffler_packages.sh
```

Once the build has completed, you can upload the results to the WMF package repository, as per the guidelines:
https://wikitech.wikimedia.org/wiki/Debian_Packaging#Upload_to_Wikimedia_Repo